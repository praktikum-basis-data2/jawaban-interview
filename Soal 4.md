## SOAL 4
Mampu mendemonstrasikan DATA QUERY LANGUAGE (DQL) berdaraskan minimal 5 pertanyaan analisis dari produk digital yang dipilih, menggunakan setidaknya keyword GROUP BY, INNER JOIN, LEFT / RIGHT JOIN, AVERAGE / MAX / MIN. Lampirkan bukti berupa source code dan screen record.

1. Bagaimana cara melihat type paling banyak di beli
```sql
SELECT type_name, COUNT(*) AS total_users
FROM users
JOIN type ON users.type_id = type.type_id
GROUP BY type_name
ORDER BY total_users desc;
```


-2. Berapa pedaptan dari pembelian semmua account
```sql
SELECT type_name, sum(harga) as total_pembelian
FROM `type`
GROUP by type_name
order by total_pembelian desc ;
```

3. bagaimana cara melihat user yaang paling banyk memiliki baanyak folder
```sql
SELECT u.username, COUNT(*) AS total_folders
FROM users u
JOIN folders f ON f.user_id = u.user_id
GROUP BY u.username
order by total_folders desc;
```


4. User paling banyak berasal dari negara mana
```sql
SELECT us.language AS country, COUNT(*) AS total_users
FROM Users u
JOIN UserSettings us ON u.user_id = us.user_id
GROUP BY us.language
ORDER BY total_users desc;
```


5. Bagaimana cara melihat Max storage dari userr terntentu
```sql
SELECT u.username, t.max_storage AS Max_Storage_GB
FROM users u
INNER JOIN `type` t ON t.type_id = u.type_id
ORDER BY t.max_storage;
```


6. Pendapatan pertahun
```sql
SELECT year(u.registration_date), sum(t.harga) as total_perbulan
FROM `type` t 
JOIN users u  ON u.type_id  = t.type_id 
GROUP BY year(u.registration_date) 
order by total_perbulan desc;
```


7. Pendapatan Perbulan
```sql
SELECT MONTHNAME(u.registration_date) AS Bulan,
       YEAR(u.registration_date) AS Tahun,
       SUM(t.harga) AS total_perbulan
FROM `type` t
JOIN users u ON u.type_id = t.type_id
GROUP BY bulan, Tahun
ORDER BY tahun, month(u.registration_date);
```
