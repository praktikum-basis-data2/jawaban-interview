## SOAL 1
Mampu mendemonstrasikan perancangan basis data berdasarkan permasalahan dunia nyata melalui reverse engineering produk digital global. Lampirkan bukti tabel hasil desain


Saya mereverse engineering produk OneDrive. OneDrive Merupakan produk yang memiliki produk bisnis menyimpan data saya melakukan reverse engineering database dari produk ini dengan membuat 16 table, diantaranya:
1. Tabel "Users" untuk menyimpan informasi pengguna
2. Tabel "Files" untuk menyimpan informasi file
3. Tabel "Folders" untuk menyimpan informasi folder
4. Tabel "SharedFolders" untuk menyimpan informasi folder yang dibagikan
5. Tabel "Permissions" untuk menyimpan informasi izin akses
6. Tabel "FilePermissions" untuk menyimpan informasi izin akses file
7. Tabel "FolderPermissions" untuk menyimpan informasi izin akses folder
8. Tabel "Trash" untuk menyimpan informasi file yang terhapus:
9. Tabel "Tags" untuk menyimpan informasi tag pada file
10. Tabel "FileTags" untuk menghubungkan file dengan tag
11. Tabel "ActivityLog" untuk menyimpan log aktivitas pengguna
12. Tabel "StorageQuota" untuk menyimpan batasan kapasitas penyimpanan pengguna
13.Tabel "SharingLinks" untuk menyimpan informasi tautan berbagi
14. Tabel "Notifications" untuk menyimpan notifikasi pengguna
15. Tabel "UserSettings" untuk menyimpan pengaturan pengguna
16. Tabel "Type" Untuk menyimpan Type pengguna

#### Berikut Merupakan bukti tabel hasil desain dengan relasi
![Dokumentasi](Dokumentasi/Diagrams_Database.png)
