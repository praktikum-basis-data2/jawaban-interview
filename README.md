## Use Case Onedrive
| No. | Use Case                                                                                          | Nilai |
|-----|---------------------------------------------------------------------------------------------------|-------|
| 1.  | Mengunggah file dari perangkat lokal ke OneDrive.                                                 | 10     |
| 2.  | Melkukan registerUser                                                                             | 10     |
| 3.  | Membuat Folder                                                                                    | 10     |
| 4.  | Menampilkan dan mengatur folder dan file di OneDrive.                                             | 9     |
| 5.  | Mengedit file langsung di OneDrive.                                                               | 8     |
| 6.  | Mengunduh file dari OneDrive ke perangkat lokal.                                                  | 9     |
| 7.  | Menghapus file yang tidak diperlukan dari OneDrive.                                               | 8     |
| 8.  | Membuat folder baru di OneDrive.                                                                  | 7     |
| 9.  | Mengganti nama file dan folder di OneDrive.                                                       | 7     |
| 10. | Mencari file dan folder di OneDrive.                                                              | 8     |
| 11. | Membuat salinan file di OneDrive.                                                                 | 8     |
| 12. | Membuka dan melihat file di OneDrive.                                                             | 8     |
| 13. | Mengatur hak akses dan izin berbagi untuk file dan folder di OneDrive.                            | 9     |
| 14. | Mengatur file dan folder dengan kategori dan tag di OneDrive.                                     | 7     |
| 15. | Mengelola versi file di OneDrive.                                                                 | 8     |
| 16. | Membatalkan tindakan atau pemulihan file yang dihapus di OneDrive.                                | 8     |
| 17. | Mengatur pemberitahuan dan notifikasi untuk aktivitas di OneDrive.                                | 7     |
| 18. | Mengarsipkan file dan folder di OneDrive.                                                         | 7     |
| 19. | Mengatur pemulihan file yang hilang atau terhapus di OneDrive.                                    | 8     |
| 20. | Mengedit metadata file dan folder di OneDrive.                                                    | 7     |
| 21. | Menandai file dan folder favorit di OneDrive untuk akses cepat.                                   | 6     |
| 22. | Mengatur dan mengelola album foto di OneDrive.                                                    | 8     |
| 23. | Mengelola file audio dan video di OneDrive.                                                       | 7     |
| 24. | Mengelola dan mengedit dokumen di OneDrive.                                                       | 9     |
| 25. | Mengelola file PDF di OneDrive.                                                                   | 8     |
| 26. | Mengorganisir file dan folder dengan fitur tata letak di OneDrive.                                | 7     |
| 27. | Mengelola file yang dibagikan oleh pengguna lain di OneDrive.                                     | 8     |
| 28. | Mengamankan file dengan enkripsi di OneDrive.                                                     | 9     |
| 29. | Menggunakan OneDrive sebagai cadangan dan penyimpanan file.                                       | 9     |
| 30. | Mengelola perangkat yang terhubung dengan OneDrive.                                               | 7     |
| 31. | Mengatur jadwal sinkronisasi OneDrive dengan perangkat lokal.                                     | 8     |
| 32. | Mengelola batasan ruang penyimpanan di OneDrive.                                                  | 7     |
| 33. | Menggabungkan dan membagikan folder di OneDrive.                                                  | 8     |
| 34. | Mengimpor file dari layanan cloud lain ke OneDrive.                                               | 8     |
| 35. | Mengintegrasikan OneDrive dengan aplikasi pihak ketiga.                                           | 8     |
| 36. | Melihat riwayat aktivitas dan log di OneDrive.                                                    | 7     |
| 37. | Mengelola akses jarak jauh ke OneDrive.                                                           | 8     |
| 38. | Menggunakan OneDrive di perangkat mobile.                                                         | 9     |
| 39. | Mengatur tautan berbagi yang memiliki tanggal kedaluwarsa di OneDrive.                            | 7     |
| 40. | Mengatur folder dan file dengan pemindahan batch di OneDrive.                                     | 8     |

## Diagram
![Dokumentasi](Dokumentasi/Diagrams_Database.png)

## Soal Interview UTS
NO | Pertanyaan | Jawaban
---|---|---
1 | Mampu mendemonstrasikan perancangan basis data berdasarkan permasalahan dunia nyata melalui reverse engineering produk digital global. Lampirkan bukti tabel hasil desain | [Jawaban 1](https://gitlab.com/praktikum-basis-data2/jawaban-interview/-/blob/main/Soal%201.md#soal-1)
2 | Mampu mendemonstrasikan DATA DEFINITION LANGUAGE (DDL) secara tepat berdasarkan minimal 10 entitas dari produk digital yang dipilih. Lampirkan bukti berupa source code dan screen record. | [Jawaban 2](https://gitlab.com/praktikum-basis-data2/jawaban-interview/-/blob/main/Soal%202.md#soal-2)
3 | Mampu mendemonstrasikan DATA MANIPULATION LANGUAGE (DML) berdaraskan minimal 5 use case operasional (CRUD) dari produk digital yang dipilih. Lampirkan bukti berupa source code dan screen record. | [Jawaban 3](https://gitlab.com/praktikum-basis-data2/jawaban-interview/-/blob/main/Soal%203.md#soal-3)
4 | Mampu mendemonstrasikan DATA QUERY LANGUAGE (DQL) berdaraskan minimal 5 pertanyaan analisis dari produk digital yang dipilih, menggunakan setidaknya keyword GROUP BY, INNER JOIN, LEFT / RIGHT JOIN, AVERAGE / MAX / MIN. Lampirkan bukti berupa source code dan screen record. | [Jawaban 4](https://gitlab.com/praktikum-basis-data2/jawaban-interview/-/blob/main/Soal%204.md#soal-4)
5 | Mampu mendemonstrasikan keseluruhan DDL, DML, dan DQL dari produk digital yang dipilih dalam bentuk video publik di Youtube. Lampirkan link Youtube | Menyusul
