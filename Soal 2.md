## Soal 2
Mampu mendemonstrasikan DATA DEFINITION LANGUAGE (DDL) secara tepat berdasarkan minimal 10 entitas dari produk digital yang dipilih. Lampirkan bukti berupa source code dan screen record.

1. Tabel "Users" untuk menyimpan informasi pengguna:

```sql
CREATE TABLE Users (
    user_id INT PRIMARY KEY,
    username VARCHAR(50) NOT NULL,
    email VARCHAR(100) NOT NULL,
    password VARCHAR(100) NOT NULL,
    registration_date DATE
    type_id INT,
    FOREIGN KEY (type_id) REFERENCES Type (type_id)
);
```

2. Tabel "Files" untuk menyimpan informasi file:

```sql
CREATE TABLE Files (
    file_id INT PRIMARY KEY,
    file_name VARCHAR(100) NOT NULL,
    file_size INT,
    upload_date DATETIME,
    user_id INT,
    FOREIGN KEY (user_id) REFERENCES Users(user_id)
);
```

3. Tabel "Folders" untuk menyimpan informasi folder:

```sql
CREATE TABLE Folders (
    folder_id INT PRIMARY KEY,
    folder_name VARCHAR(100) NOT NULL,
    parent_folder_id INT,
    user_id INT,
    FOREIGN KEY (parent_folder_id) REFERENCES Folders(folder_id),
    FOREIGN KEY (user_id) REFERENCES Users(user_id)
);
```

4. Tabel "SharedFolders" untuk menyimpan informasi folder yang dibagikan:

```sql
CREATE TABLE SharedFolders (
    shared_folder_id INT PRIMARY KEY,
    folder_id INT,
    user_id INT,
    FOREIGN KEY (folder_id) REFERENCES Folders(folder_id),
    FOREIGN KEY (user_id) REFERENCES Users(user_id)
);
```

5. Tabel "Permissions" untuk menyimpan informasi izin akses:

```sql
CREATE TABLE Permissions (
    permission_id INT PRIMARY KEY,
    permission_name VARCHAR(50) NOT NULL
);
```

6. Tabel "FilePermissions" untuk menyimpan informasi izin akses file:

```sql
CREATE TABLE FilePermissions (
    file_id INT,
    user_id INT,
    permission_id INT,
    FOREIGN KEY (file_id) REFERENCES Files(file_id),
    FOREIGN KEY (user_id) REFERENCES Users(user_id),
    FOREIGN KEY (permission_id) REFERENCES Permissions(permission_id)
);
```

7. Tabel "FolderPermissions" untuk menyimpan informasi izin akses folder:

```sql
CREATE TABLE FolderPermissions (
    folder_id INT,
    user_id INT,
    permission_id INT,
    FOREIGN KEY (folder_id) REFERENCES Folders(folder_id),
    FOREIGN KEY (user_id) REFERENCES Users(user_id),
    FOREIGN KEY (permission_id) REFERENCES Permissions(permission_id)
);
```

8. Tabel "Trash" untuk menyimpan informasi file yang terhapus:

```sql
CREATE TABLE Trash (
    trash_id INT PRIMARY KEY,
    file_id INT,
    delete_date DATETIME,
    FOREIGN KEY (file_id) REFERENCES Files(file_id)
);
```

9. Tabel "Tags" untuk menyimpan informasi tag pada file:

```sql
CREATE TABLE Tags (
    tag_id INT PRIMARY KEY,
    tag_name VARCHAR(50) NOT NULL
);
```

10. Tabel "FileTags" untuk menghubungkan file dengan tag:

```sql
CREATE TABLE FileTags (
    file_id INT,
    tag_id INT,
    FOREIGN KEY (file_id) REFERENCES Files(file_id),
    FOREIGN KEY (tag_id) REFERENCES Tags(tag_id)
);
```

11. Tabel "ActivityLog" untuk menyimpan log aktivitas pengguna:

```sql
CREATE TABLE ActivityLog (
    log_id INT PRIMARY KEY,
    user_id INT,
    activity VARCHAR(100) NOT NULL,
    log_date DATETIME,
    FOREIGN KEY (user_id) REFERENCES Users(user_id)
);
```

12. Tabel "StorageQuota" untuk menyimpan batasan kapasitas penyimpanan pengguna:



```sql
CREATE TABLE StorageQuota (
    user_id INT,
    max_storage INT,
    used_storage INT,
    FOREIGN KEY (user_id) REFERENCES Users(user_id)
);
```

13. Tabel "SharingLinks" untuk menyimpan informasi tautan berbagi:

```sql
CREATE TABLE SharingLinks (
    link_id INT PRIMARY KEY,
    file_id INT,
    link VARCHAR(100) NOT NULL,
    expiry_date DATE,
    FOREIGN KEY (file_id) REFERENCES Files(file_id)
);
```

14. Tabel "Notifications" untuk menyimpan notifikasi pengguna:

```sql
CREATE TABLE Notifications (
    notification_id INT PRIMARY KEY,
    user_id INT,
    message VARCHAR(100) NOT NULL,
    notification_date DATETIME,
    FOREIGN KEY (user_id) REFERENCES Users(user_id)
);
```

15. Tabel "UserSettings" untuk menyimpan pengaturan pengguna:

```sql
CREATE TABLE UserSettings (
    user_id INT,
    theme VARCHAR(50) NOT NULL,
    language VARCHAR(50) NOT NULL,
    PRIMARY KEY (user_id, theme, language),
    FOREIGN KEY (user_id) REFERENCES Users(user_id)
);
```
16. Tabel "Type" Untuk menyimpan Type pengguna:
```sql
CREATE TABLE Type (
    type_id INT PRIMARY KEY,
    type_name VARCHAR(50) NOT NULL,
    max_storage INT,
    harga INT
);
```
